package com.java.programs;

public class PrimeNumber {
    public static void  main(String[] args)
    {
        int num =  7;
        boolean primeFlag = true;
        if(num ==0|| num ==1)
        {
            System.out.println(num + " not a prime number.");
        }
        else {

            for(int i =2; i <= num/2; i++)
            {
                if(num % i ==  0)
                {
                    System.out.println(num + " not a Prime number.");

                    primeFlag = false;
                    break;
                }
            }
            if(primeFlag)
            {
                System.out.println(num + " Prime number.");
            }


        }
    }
}
