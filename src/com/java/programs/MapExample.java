package com.java.programs;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class MapExample {

    public static void main(String[] args)
    {
        Map<Integer, String> hmList = new HashMap<Integer, String>();
        hmList.put(1,"shubha");
        hmList.put(2,"Shweta");
        hmList.put(3,"mala");
        hmList.put(4,"harika");
        hmList.put(null,null);


        for(Map.Entry e:hmList.entrySet())
        {
            System.out.println(e.getKey() + " " + e.getValue());
        }

        System.out.println("------------------");

        TreeMap<Integer, String> tMap = new TreeMap<Integer, String>();

        tMap.put(0,"vegetables");
        tMap.put(1,"Carrot");
        tMap.put(2,null);
        tMap.put(3,"Lady Finger");

        for(Map.Entry e:tMap.entrySet())
        {
            System.out.println(e.getKey() + " " + e.getValue());
        }



    }
}
