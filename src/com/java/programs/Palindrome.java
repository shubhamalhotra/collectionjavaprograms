package com.java.programs;

public class Palindrome {
    public static void main(String[] args)
    {
        String palindromeString = "1234321";

        int len = palindromeString.length();
        int last = len-1;
        char firstChar, lastChar;
        boolean result = true;
        for(int first=0; first < palindromeString.length(); first++)
        {
                firstChar = palindromeString.charAt(first);
                lastChar = palindromeString.charAt(last);
        last--;
                if(firstChar == lastChar) {

                    continue;

                }
                else {
                    result = false;

                    break;

                }

        }
        System.out.println(result);
    }
}
