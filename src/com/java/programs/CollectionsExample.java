package com.java.programs;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class CollectionsExample {

    public static void main(String[] args)
    {
        List list = new LinkedList();

        list.add("shubha");
        list.add("mala");
        list.add("shweta");


        Iterator iterator = list.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }


        //System.out.println((list));

        Vector  listVector = new Vector();

        listVector.add("car");
        listVector.add("truck");
        listVector.add("bus");

        Iterator itVector = listVector.iterator();
        while(itVector.hasNext()) {
            System.out.println(itVector.next());
        }

    }

}
