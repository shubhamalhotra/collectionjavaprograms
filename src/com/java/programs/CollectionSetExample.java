package com.java.programs;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class CollectionSetExample {

    public static void main(String[] args)
    {
        Set family = new TreeSet();
        family.add("grandFather");
        family.add("father");
        family.add("mother");
        family.add("child");

    family.remove("mother");
        Iterator itTreeSet = family.iterator();
        while(itTreeSet.hasNext()) {
            System.out.println(itTreeSet.next());
            itTreeSet.remove();
        }

        System.out.println("*****");
        Set linkHashList = new LinkedHashSet();
        linkHashList.add(10);
        linkHashList.add(20);
        linkHashList.add(30);
        linkHashList.add(40);
        linkHashList.add(50);

        linkHashList.remove(30);
        Iterator itLinkHash = linkHashList.iterator();
        while(itLinkHash.hasNext()) {
            System.out.println(itLinkHash.next());
        }


    }
}
